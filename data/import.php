<?php
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('Asia/Hong_Kong');

require_once 'Classes/PHPExcel/IOFactory.php';

echo EOL, EOL;

//
//	DB setup and config
//
require_once '../api/rb.php';
R::setup("mysql:host=localhost;dbname=sp","root","");
R::$writer->setUseCache(true);
echo "Importing spreadsheets into database" . EOL;




R::wipe("client");
$file = 'client.xlsx';
echo "Loading spreadsheet - please be patient" . EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setReadDataOnly(true);
$objPHPExcel = $objReader->load($file);
processClient(0);

R::wipe("item");
$file = 'item.xlsx';
echo "Loading spreadsheet - please be patient" . EOL;
$objReader = PHPExcel_IOFactory::createReader('Excel2007');
$objReader->setReadDataOnly(true);
$objPHPExcel = $objReader->load($file);
processItem(0);


function cleanField($f) {
	$f = trim($f);
	$f = preg_replace('/\n/','',$f);
	$f = preg_replace('/\r/','',$f);
	return $f;
}

function processClient($sheet)
{
	echo "Processing Client spreadsheet" . EOL;
	global $objPHPExcel;
	
	$objWorksheet = $objPHPExcel->setActiveSheetIndex($sheet);

	$col = 0;
	$row = 1;

	$totcols = 21;

	$brand = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());

	while (strlen($brand) > 0)
	{
		$mrcode = cleanField($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
		$code = cleanField($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
		$name = cleanField($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
		$contact = cleanField($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());
		$address = cleanField($objWorksheet->getCellByColumnAndRow(4, $row)->getValue());
		$area = cleanField($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
		$tel1 = cleanField($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
		$tel2 = cleanField($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
		$fax = cleanField($objWorksheet->getCellByColumnAndRow(8, $row)->getValue());
		$email = cleanField($objWorksheet->getCellByColumnAndRow(9, $row)->getValue());
		$deliverycar = cleanField($objWorksheet->getCellByColumnAndRow(10, $row)->getFormattedValue());
		$sort = cleanField($objWorksheet->getCellByColumnAndRow(11, $row)->getValue());
		$credit = cleanField($objWorksheet->getCellByColumnAndRow(12, $row)->getValue());
		$salesman = cleanField($objWorksheet->getCellByColumnAndRow(13, $row)->getValue());
		$cashcredit = cleanField($objWorksheet->getCellByColumnAndRow(14, $row)->getValue());
		$remarks = cleanField($objWorksheet->getCellByColumnAndRow(15, $row)->getValue());
		$coflag = cleanField($objWorksheet->getCellByColumnAndRow(16, $row)->getValue());
		$remarks2 = cleanField($objWorksheet->getCellByColumnAndRow(17, $row)->getValue());
		$remarks3 = cleanField($objWorksheet->getCellByColumnAndRow(18, $row)->getValue());
		$remarks4 = cleanField($objWorksheet->getCellByColumnAndRow(19, $row)->getValue());
		$remarks5 = cleanField($objWorksheet->getCellByColumnAndRow(20, $row)->getValue());

		#$firm = preg_replace('/\&/',';',$firm);
		#$firm = preg_replace('/ */','',$firm);
		#$arrfirms = explode ( ";" ,  $firm );
		$client = R::findOne('client',' code = ? ', 
			array( $code )
			);

		//if (strlen($client['code']) > 3) {
		//	print " EXISTS : ";
//$client['qcpassword']=$password;
//$client['cname']=$cname;
//$id = R::store($client);
		//} else {
			print " ADD    : ";
			$client = R::dispense('client');
			$client['mrcode']=$mrcode;
			$client['code']=$code;
			$client['name']=$name;
			$client['contact']=$contact;
			$client['address']=$address;
			$client['area']=$area;
			$client['tel1']=$tel1;
			$client['tel2']=$tel2;
			$client['fax']=$fax;
			$client['email']=$email;
			$client['deliverycar']=$deliverycar;
			$client['sort']=$sort;
			$client['credit']=$credit;
			$client['salesman']=$salesman;
			$client['cashcredit']=$cashcredit;
			$client['remarks']=$remarks;
			$client['coflag']=$coflag;
			$client['remarks2']=$remarks2;
			$client['remarks3']=$remarks3;
			$client['remarks4']=$remarks4;
			$client['remarks5']=$remarks5;
			$id = R::store($client);

		  //}

		print $row++ . ",";
		print $mrcode . ",";
		print $code . ",";
		print $name . ",";
		print EOL;
		
		
		$brand = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
	}

	print "End of File at row " . $row . EOL;
}

function processItem($sheet)
{
	echo "Processing Item spreadsheet" . EOL;
	global $objPHPExcel;
	
	$objWorksheet = $objPHPExcel->setActiveSheetIndex($sheet);

	$col = 0;
	$row = 1;

	$totcols = 21;

	$brand = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());

	while (strlen($brand) > 0)
	{
		$code = cleanField($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
		$desc = cleanField($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
		$unit = cleanField($objWorksheet->getCellByColumnAndRow(2, $row)->getValue());
		$price = cleanField($objWorksheet->getCellByColumnAndRow(3, $row)->getValue());


		$item = R::findOne('item',' code = ? ', 
			array( $code )
			);

		if (strlen($item['code']) > 3) {
			print " EXISTS : ";
		} else {
			print " ADD    : ";
			$item = R::dispense('item');
			$item['code']=$code;
			$item['desc']=$desc;
			$item['unit']=$unit;
			$item['price']=$price;

			$id = R::store($item);

		  }

		print $row++ . ",";
		print $code . ",";
		print $desc . ",";
		print $unit . ",";
		print $price . ",";
		print EOL;
		
		
		$brand = trim($objWorksheet->getCellByColumnAndRow(0, $row)->getValue());
	}

	print "End of File at row " . $row . EOL;
}
