angularApp.controller('loginController', function($rootScope, $scope, api, $routeParams, $location, Restangular, Auth) {

  $rootScope.page = 'Login';

  $scope.login = function () {

    var d = { 
              'userid': $scope.username,
              'password': $scope.password
            };

    api.login(d).then(function (data) {

      if (data.item != '0') {
        Auth.setUser(data.item); 

        $location.path('/customer');
      } 
      else {
        alert('Invalid username and password');
      }

    });
    
  };  

});