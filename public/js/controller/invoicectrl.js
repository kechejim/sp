angularApp.controller('invoiceController', function($filter, $rootScope, $scope, $controller, $http, api, $routeParams, Restangular) {

  var date = new Date();  
  $scope.today = date.getFullYear().toString() + "-"+ ("0" + (date.getMonth() + 1).toString() ).substr(-2) + "-" + ("0" + date.getDate().toString()).substr(-2);
   
  $scope.remarks1 = "";
  $scope.remarks2 = "";
  $scope.remarks3 = "";
  $scope.totalPrice = 0;
  $scope.issuedDate = $scope.today;
  $scope.cashcredit = "";
  
  if ($rootScope.selectedClient) {
    $scope.remarks1 = $rootScope.selectedClient.remarks1;
    $scope.remarks2 = $rootScope.selectedClient.remarks2;
    $scope.remarks3 = $rootScope.selectedClient.remarks3;
    $scope.cashcredit = $rootScope.selectedClient.cashcredit;
  };
  
  //configure grid        
  $scope.$on('ngGridEventEndCellEdit', function(evt){
      //console.log(evt.targetScope.row.entity);  // the underlying data bound to the row
      
      var obj = evt.targetScope.row.entity;
      var sum = 0;

      for (var j=0;j< $rootScope.items.length;j++ )
      {
        sum += $rootScope.items[j].qty * $rootScope.items[j].price;  
        if (obj.id == $rootScope.items[j].id) {
            $rootScope.items[j].total = $rootScope.items[j].qty * $rootScope.items[j].price;
        };
      } 

      $scope.totalPrice = sum;
      $scope.$apply();      
  });
  
  $scope.gridOptions = { 
    data: 'items',  
    showSelectionCheckbox: true,
    enableCellSelection: true,
    enableRowSelection: true,      
    selectedItems: [],
    multiSelect: true,
    columnDefs: [
          {field:'code', displayName:'Code', enableCellEdit:false, width:'10%'},
          {field: 'item', displayName: 'Item', enableCellEdit: false, width:'30%'}, 
          {field:'qty', displayName:'Qty', enableCellEdit: true, width:'10%'},
          {field:'unit', displayName:'Unit', enableCellEdit: false, width:'10%'},
          {field:'price', displayName:'Price', enableCellEdit: true, width:'20%'},
          {field:'total', displayName:'Total', enableCellEdit: false, width:'20%'}
        ]
  };

  $scope.$watch('remarksinfo', function(value) {
      
      switch (value) {
        case 'remarks1':
          $scope.tmpRemark = $scope.remarks1;
          break;
        case 'remarks2':
          $scope.tmpRemark = $scope.remarks2;
          break;
        case 'remarks3':
          $scope.tmpRemark = $scope.remarks3;
          break;
      }
  });

  $scope.$watch('tmpRemark', function(value) {
      switch ($scope.remarksinfo) {
        case 'remarks1':
          $scope.remarks1 = value;
          break;
        case 'remarks2':
          $scope.remarks2 = value;
          break;
        case 'remarks3':
          $scope.remarks3 = value;
          break;
      }

  });

  $scope.filterUser = function(item) {
    return item.isDeleted !== true;
  };

  // mark user as deleted
  $scope.deleteUser = function(id) {
    var filtered = $filter('filter')($rootScope.items, {id: id});
    if (filtered.length) {
      filtered[0].isDeleted = true;
    }
  };

  // add user
  $scope.addUser = function() {
    $rootScope.items.push({
      id: $rootScope.items.length+1,
      code:'',
      item: '',
      qty: 0,
      unit: null,
      price: 0,
      isNew: true,
      total: 0
    });	  
  };

  $scope.removeUser = function() {

  	for (var i=0;i< $scope.gridOptions.selectedItems.length;i++ )
  	{
  		console.log('selected   : ' + $scope.gridOptions.selectedItems[i].item);
  		var id = $scope.gridOptions.selectedItems[i].id;

  		for (var j=0;j<$rootScope.items.length ;j++ )
  		{
  			if ($rootScope.items[j].id == id)
  			{
  				$rootScope.items.splice(j,1);

  				break;
  			}
  		}
  	}

  	$scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length); 

  };

  // cancel all changes
  $scope.cancel = function() {
    for (var i = $rootScope.items.length; i--;) {
      var user = $rootScope.items[i];    
      // undelete
      if (user.isDeleted) {
        delete user.isDeleted;
      }
      // remove new 
      if (user.isNew) {
        $rootScope.items.splice(i, 1);
      }      
    };
  };

  // store data into db, not print
  $scope.save = function() {

  	if (!$rootScope.selectedClient || !$rootScope.selectedTruck)
  	{
  		alert('Please select customer or truck route !');
  		return;
  	}

  	var invoice = {
  		'truckid' :	$rootScope.selectedTruck.id,
  		'clientid': $rootScope.selectedClient.id,
  		'items'   : $rootScope.items,
      'remarks1': $scope.remarks1,
      'remarks2': $scope.remarks2,
      'remarks3': $scope.remarks3,
      'total'   : $scope.totalPrice,
      'cashcredit': $scope.cashcredit,
      'tag'     : $rootScope.selectedTag.substring(0,1),
      'issuedDate' : $scope.issuedDate
  	};

  	api.saveInvoice(invoice).then(function (data) {
  	  console.log(data);

      
      $scope.Message = 'It is printing invoice.  Generated Invoice ID is ' + data;
      //$('#mini-notification').miniNotification({closeButton: false});
      alert($scope.Message);      
      
      $rootScope.lastChangedInvoiceNum = data;
      $rootScope.selectedTag = '';
      
      //reinit      
      $scope.remarks1 = "";
      $scope.remarks2 = "";
      $scope.remarks3 = "";        
      $scope.totalPrice = 0;
      $scope.issuedDate = $scope.today;

      $rootScope.items.splice(0,$rootScope.items.length);
  
    });

  }

  // when user click 'ctrl+s' 
  $scope.$on("SaveFromSortkey", function (event, args) {
    $scope.save();
  });
  
});
