angularApp.controller('truckController', function($rootScope, $scope, $controller, api, $routeParams, $location, Restangular) {

  $controller('CommonController',{$scope: $scope});

  $scope.fetchResult = function () {
    api.getTable("truckroute",$scope.filterCriteria).then(function (data) {
      $scope.data = data;
    });
  };
  $scope.fetchResult();

  $scope.selectTruck = function(item) {
    $rootScope.selectedTruck = item;
    $location.path("#/home");
  };
});

