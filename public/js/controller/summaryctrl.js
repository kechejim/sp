angularApp.controller('summaryController', function($rootScope, $scope, $controller, api, $routeParams, Restangular) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Daily Sale';
  
  var date = new Date();    
  $scope.filterCriteria.date = date.getFullYear().toString() + "-"+ ("0" + (date.getMonth() + 1).toString() ).substr(-2) + "-" + ("0" + date.getDate().toString()).substr(-2);
  
  $scope.fetchResult = function () {
     
    api.getDailySummary($scope.filterCriteria).then(function (data) {
        $scope.data = data;   
    });

  };
  $scope.fetchResult();

});
