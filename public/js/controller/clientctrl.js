angularApp.controller('clientController', function($rootScope, $scope, $controller, api, $routeParams, $location, Restangular, $q) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Main';

  $scope.fetchResult = function () {
    api.getTable("client",$scope.filterCriteria).then(function (data) {
      $scope.data = data;
    });
  };
  $scope.fetchResult();

  $scope.selectClient = function(item) {
    $rootScope.selectedClient = item;

    //get truck by client
    var criteria = { id: $rootScope.selectedClient.truck1};
    api.getTruckRoute(criteria).then(function (data) {

      $rootScope.selectedTruck = data.item;           
    });

    $location.path("/main");

  };

});