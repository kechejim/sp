//  Common Controller
angularApp.controller('CommonController', function($scope, $rootScope, api, $location, Restangular, $http) {

  $scope.filterCriteria = {
    page: 1,
    pagesize: 12,
    sortdir: 'asc',
    sortby: 'id',
    filterElement: '',
    filterElementValue: ''
  };

  //called when navigate to another page in the pagination
  $scope.selectPage = function (page) {
    $scope.data={};
    $scope.filterCriteria.page = page;
    $scope.fetchResult();
  };

  //Will be called when filtering the grid, will reset the page number to one
  $scope.filterResult = function () {
    $scope.data={};
    $scope.filterCriteria.page = 1;
    $scope.fetchResult();
  };

  $scope.onSort = function (page) {
    console.log("ISACTIVE:".page);
  }

  //call back function that we passed to our custom directive sortBy, will be called when clicking on any field to sort
  $scope.onSort = function (sortedBy, sortDir) {
    $scope.data={};
    $scope.filterCriteria.sortdir = sortDir;
    $scope.filterCriteria.sortby = sortedBy;
    $scope.filterCriteria.page = 1;
    $scope.fetchResult();
  };

}); 
