angularApp.controller('previewController', function($rootScope, $scope, $controller, api, $routeParams, Restangular, $timeout) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Preview';
  
  $scope.$on('ngGridEventEndCellEdit', function(evt){
      //console.log(evt.targetScope.row.entity);  // the underlying data bound to the row
      
      var obj = evt.targetScope.row.entity;
      var sum = 0;

      for (var j=0;j< $rootScope.invoicePreview.items.length;j++ )
      {
        sum += $rootScope.invoicePreview.items[j].qty * $rootScope.invoicePreview.items[j].price;  
        if (obj.id == $rootScope.invoicePreview.items[j].id) {
            $rootScope.invoicePreview.items[j].total = $rootScope.invoicePreview.items[j].qty * $rootScope.invoicePreview.items[j].price;
        };
      } 

      $rootScope.invoicePreview.remarks.totalprice = sum;
      $scope.$apply();      
  });
  
  $scope.gridOptions = { 
    data: 'invoicePreview.items',  
    showSelectionCheckbox: true,
    enableCellSelection: true,
    /*enableRowSelection: true,*/      
    selectedItems: [],
    multiSelect: true,
    columnDefs: [
          {field:'code', displayName:'Code', enableCellEdit:true, width:'10%'},
          {field: 'item', displayName: 'Item', enableCellEdit: true, width:'30%'}, 
          {field:'qty', displayName:'Qty', enableCellEdit: true, width:'10%'},
          {field:'unit', displayName:'Unit', enableCellEdit: true, width:'10%'},
          {field:'price', displayName:'Price', enableCellEdit: true, width:'20%'},
          {field:'total', displayName:'Total', enableCellEdit: false, width:'20%'}
        ]
  };
   
  $scope.$on('$viewContentLoaded', function() {
    
    $timeout(function() {
      window.print();
    }, 500, false);
    
  });
 
});
