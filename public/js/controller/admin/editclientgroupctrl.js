angularApp.controller('editClientGroupController', function($rootScope, $scope, $controller, api, $routeParams, $location, Restangular) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Edit Client Group';
  
  //init
  $scope.selectedClient = '';
  $scope.mode = 'new';

  $scope.fetchResult = function () {
    api.getTable("clientgroup",$scope.filterCriteria).then(function (data) {
      $scope.data = data;
    });
  };
  $scope.fetchResult();
  
  $scope.selectClientGroup = function(cl) {  	
  	$scope.selectedClientGroup = cl; 
    $scope.mode = 'update';
  };

  //Creat/Update/Delete
  $scope.new = function() {
    $scope.selectedClientGroup = '';
    $scope.mode = 'new';
  };

  $scope.save = function() {
  	
    if ($scope.selectedClientGroup == '') {
      return;
    }

    var d = {              
              clientgroup: $scope.selectedClientGroup
            };
    
    api.createClientGroup(d).then(function (data) {    
      alert('Created successfully !');      

    });

  };

  $scope.update = function() {
    
    var d = {              
              clientgroup: $scope.selectedClientGroup
            };

    api.updateClientGroup(d).then(function (data) {    
      
      alert('Updated successfully !');      
    });
  };

  $scope.delete = function() {
    
    var d = {              
              clientgroup: $scope.selectedClientGroup
            };

    api.deleteClientGroup(d).then(function (data) {    
      
      alert('Deleted successfully !');      
    });
  };

});
