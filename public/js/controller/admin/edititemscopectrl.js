angularApp.controller('editItemScopeController', function($rootScope, $scope, api, $routeParams, $location, Restangular,  $filter, ngTableParams) {

  $rootScope.page = 'Mass Edit Item Scope';
  
  /* checked itemlist */
  $scope.checkedItemlist = [];
  $rootScope.itemStatus = []; //temporary

  api.getTable("item",{}).then(function (data) {      
    $scope.AllItemtList = data.items;      
  });


  $scope.itemcheckedAll = function(e) {
    
    if (e) {      
      for (var i=0; i<$scope.AllItemtList.length; i++) {
        var el = $scope.AllItemtList[i];            
        $scope.checkedItemlist.push(el);   
        $rootScope.itemStatus[el.id] = true;     
      }      

    } else {
      for (var i=0; i<$scope.checkedItemlist.length; i++) {
        var el = $scope.checkedItemlist[i];                    
        $rootScope.itemStatus[el.id] = false;     
      }     

      $scope.checkedItemlist.splice(0, $scope.checkedItemlist.length);        
    }        
  };

  $scope.itemchecked = function(el, status, bClick) {
    if (bClick == 1) {
      status = !status;
      $rootScope.itemStatus[el.id] = !$rootScope.itemStatus[el.id];
    }

  	if (status == true) {
      
      el.price = 0;

  		$scope.checkedItemlist.push(el);
  	} else {

  		for (var i=0; i<$scope.checkedItemlist.length; i++) {
  			if (el.id == $scope.checkedItemlist[i].id) {
  				$scope.checkedItemlist.splice(i, 1);	
  				break;
  			} 			
  		}
  	}  	
   
  };

  /* checked clientlist*/  
  $scope.checkedClientlist = [];
  $rootScope.clientStatus = []; //temporary
  $scope.selectedClientGroup = '0';
  $scope.AllClientList = [];
  
  $scope.$on("ClientGroupSelected", function (event, args) {
    $scope.selectedClientGroup = args.clientgroup;
    $rootScope.$broadcast("AllClientChecked", {value: false });  

    api.getTable("client",{filterElement:'clientgroup', filterElementValue:args.clientgroup }).then(function (data) {
      
      $scope.AllClientList = data.items;
      
    });

  });

  $scope.clientchecked = function(el, status, bClick) {
    if (bClick == 1) {
      status = !status;
      $rootScope.clientStatus[el.id] = !$rootScope.clientStatus[el.id];
    }

    if (status == true) {
      
      el.price = 0;

      $scope.checkedClientlist.push(el);
    } else {

      for (var i=0; i<$scope.checkedClientlist.length; i++) {
        if (el.id == $scope.checkedClientlist[i].id) {
          $scope.checkedClientlist.splice(i, 1);  
          break;
        }       
      }
    }

    $scope.selectedClientTB.reload();  
  };

  $scope.clientcheckedAll = function(e) {
    if ($scope.selectedClientGroup == '0') {
      $rootScope.$broadcast("AllClientChecked", {value: false });
    }

    if (e) {      
      for (var i=0; i<$scope.AllClientList.length; i++) {
        var el = $scope.AllClientList[i];            
        $scope.checkedClientlist.push(el);   
        $rootScope.clientStatus[el.id] = true;     
      }      

    } else {
      for (var i=0; i<$scope.checkedClientlist.length; i++) {
        var el = $scope.checkedClientlist[i];                    
        $rootScope.clientStatus[el.id] = false;     
      }     

      $scope.checkedClientlist.splice(0, $scope.checkedClientlist.length);        
    }
    
    $scope.selectedClientTB.reload();
  };

  $scope.clearClientCheckedList = function() {
       
    $rootScope.$broadcast("AllClientChecked", {value: false });   

    for (var i=0; i<$scope.checkedClientlist.length; i++) {
      var el = $scope.checkedClientlist[i];                    
      $rootScope.clientStatus[el.id] = false;     
    }     

    $scope.checkedClientlist.splice(0, $scope.checkedClientlist.length);  
    $scope.selectedClientTB.reload();
  };

  //----------------------------------------------------
  // save mapping of client list and item list
  //----------------------------------------------------
  $scope.save = function() {
  	if ($scope.selectedClientGroup == '') {
      alert('Please select clientgroup !');
      return;
    }

    var d = {
        clientgroup: $scope.selectedClientGroup,
        clientList: $scope.checkedClientlist,
        itemList: $scope.checkedItemlist
    };

    api.saveItemScope(d).then(function (data) {

      alert('Items are added successfully !');     

    });
  };

  $scope.gridOptions = { 
    data: 'checkedItemlist',  
    showSelectionCheckbox: true,
    enableCellSelection: true,
    enableRowSelection: true,      
    selectedItems: [],
    multiSelect: true,
    filterOptions: {filterText: '', useExternalFilter: false},
    showFilter: true,

    columnDefs: [
          {field:'code', displayName:'Code', enableCellEdit:false, width:'20%'},
          {field:'desc', displayName: 'Desc', enableCellEdit: false, width:'30%'}, 
          {field:'unit', displayName:'Unit', enableCellEdit: false, width:'10%'},
          {field:'tag', displayName:'Tag', enableCellEdit: false, width:'20%'},
          {field:'price', displayName:'Price', enableCellEdit: true, width:'20%'}
        ]
  };

  $scope.removeItems = function() {
    
    for (var i=0;i< $scope.gridOptions.selectedItems.length;i++ )
    {      
      var id = $scope.gridOptions.selectedItems[i].id;

      for (var j=0;j<$scope.checkedItemlist.length ;j++ )
      {
        if ($scope.checkedItemlist[j].id == id)
        {
          $scope.checkedItemlist.splice(j,1);
          $rootScope.itemStatus[id] = false;

          break;
        }
      }
    }

    $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length); 

  };
  
  $scope.selectedClientTB = new ngTableParams({
        page: 1,            // show first page
        count: 8,          // count per page        
        filter : {
          code: '',
          name: '',
          deliverycar: ''          
        }
    }, {
        total: $scope.checkedClientlist.length, // length of data
        getData: function($defer, params) {
            // use build-in angular filter
            var orderedData = params.filter() ?
                   $filter('filter')($scope.checkedClientlist, params.filter()) :
                   $scope.checkedClientlist;

            $scope.data = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count());

            params.total(orderedData.length); // set total for recalc pagination
            $defer.resolve($scope.data);
        }
    }
  );

});
