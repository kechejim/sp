angularApp.controller('editItemController', function($rootScope, $scope,$controller, api, $routeParams, $location, Restangular) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Edit Item';
  
  //init
  $scope.selectedItem = '';
  $scope.mode = 'new';

  //get unit list
  api.getTable("unit", {}).then(function (data) {      
      $scope.units = data.items;
  });
  //get tag list
  api.getTable("tag", {}).then(function (data) {      
      $scope.tags = data.items;
  });
  //get country list
  api.getTable("country", {}).then(function (data) {      
      $scope.countries = data.items;
  });

  $scope.totalServerItems = 0;
  $scope.pagingOptions = {
      pageSizes: [25, 50, 75, 100],
      pageSize: 25,
      currentPage: 1
  };  

  $scope.setPagingData = function(data, page, pageSize){  
    
    $scope.items = data.items; 
    $scope.totalServerItems = data.totalitems;

    if (!$scope.$$phase) {
        $scope.$apply();
    }
  };

  $scope.getPagedDataAsync = function (pageSize, page) {
      setTimeout(function () {
        
          $scope.mode = 'change';
          $scope.filterCriteria.page = page;
          $scope.filterCriteria.pagesize = pageSize;
          $scope.filterCriteria.hidden = 0;

          api.getTable("item",$scope.filterCriteria ).then(function (data) {
                console.log(data.sql);

                $scope.setPagingData(data, page, pageSize);
              });

      }, 50);
  };

  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);

  $scope.$watch('pagingOptions', function (newVal, oldVal) {

    if (newVal !== oldVal && newVal.currentPage !== oldVal.currentPage) {
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }
  }, true);
  
  $scope.$watch('filterCriteria.item', function (newVal, oldVal) {    
    console.log('filteroptions ');

    if (newVal !== oldVal) {

      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);
    }
  }, true);

  $scope.gridOptions = { 
    data: 'items',      
    showSelectionCheckbox: true,
    enableCellSelection: true,
    enableRowSelection: true,      
    selectedItems: [],
    multiSelect: true,    
        
    enablePaging: true,
    showFooter: true,
    totalServerItems: 'totalServerItems',
    pagingOptions: $scope.pagingOptions,

    columnDefs: [
          {field:'code', displayName:'Code', enableCellEdit:true, width:'20%'},
          {field:'desc', displayName: 'Desc', enableCellEdit: true, width:'20%'}, 
          {field:'unit', displayName:'Unit', enableCellEdit: true, enableCellEditOnFocus: true, 
        editableCellTemplate: 'selectunit.html', width:'10%'},
          {field:'price', displayName:'Price', enableCellEdit: true, width:'15%'},          
          {field:'tag', displayName:'Tag', enableCellEdit: true,enableCellEditOnFocus: true, 
        editableCellTemplate: 'selecttag.html', width:'15%'},          
          {field:'country', displayName:'Country', enableCellEdit: true,enableCellEditOnFocus: true, 
        editableCellTemplate: 'selectcountry.html', width:'10%'}
        ]
  };
  
  $scope.new = function() {
    
    $scope.mode = 'new';
    $scope.items.splice(0,$scope.items.length);   
    $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length);   
  };

  $scope.addrow = function() {
    $scope.items.push({      
      id: $scope.items.length+1,
      code:'',
      desc: '',
      unit: '',      
      price: 0,
      tag:'',
      country: ''          
    });
  }
  
  $scope.remove = function() {
    
    if ($scope.mode == 'new') {

      for (var i=0;i< $scope.gridOptions.selectedItems.length;i++ )
      {        
        var id = $scope.gridOptions.selectedItems[i].id;

        for (var j=0;j<$scope.items.length ;j++ )
        {
          if ($scope.items[j].id == id)
          {
            $scope.items.splice(j,1);

            break;
          }
        }
      }
      $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length);   

    } else {
      var d = {
        items: $scope.gridOptions.selectedItems
      };

      api.removeItems(d).then(function (data) {    
      
        alert('Removed successfully !');

        $scope.filterCriteria.item = '';
        $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length);   
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);      
        
      });
    }
  };

  //create item
  $scope.save = function() {
  	var d = {
        items: $scope.items
      };

    api.saveItems(d).then(function (data) {    

      alert('Added successfully !');    

      $scope.mode = 'change';
      $scope.filterCriteria.item = '';
      $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length);   
      $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);      
    });

  };

  //update item
  $scope.update = function() {

      var d = {
        items: $scope.items
      };

      api.updateItems(d).then(function (data) {    

        alert('Updated successfully !');    

        $scope.pagingOptions.currentPage = 0;
        
        $scope.filterCriteria.item = '';
        $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length);   
        $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage);      
      });

  };
  

});
