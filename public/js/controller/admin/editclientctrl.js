angularApp.controller('editClientController', function($rootScope, $scope, api, $routeParams, $location, Restangular) {

  $rootScope.page = 'Mass Edit Client';
    
  //init
  $scope.selectedClient = '';
  $scope.mode = 'new';
 
  $scope.selectClient = function(cl) {  	
  	$scope.selectedClient = cl; 
    $scope.mode = 'update';
  };

  $scope.new = function() {
    $scope.selectedClient = '';
    $scope.mode = 'new';
  };

  $scope.save = function() {
  	
    if ($scope.selectedClient == '') {
      return;
    }

    var d = {              
              client: $scope.selectedClient
            };
    
    api.createClient(d).then(function (data) {    
      alert('Created successfully !');
      
    });

  };

  $scope.update = function() {
    
    var d = {              
              client: $scope.selectedClient
            };

    api.updateClient(d).then(function (data) {    
      alert('Updated successfully !');
    });
  };

});
