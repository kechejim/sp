angularApp.controller('adminitemController', function($rootScope, $scope, $controller, api, $routeParams, Restangular) {

  $controller('CommonController',{$scope: $scope});

  $scope.filterCriteria = {
    pagesize: 8
  };

  $scope.fetchResult = function () {
   
    $scope.filterCriteria.orderCond = 'frequency';
    $scope.filterCriteria.hidden = 0;

    api.getTable("item",$scope.filterCriteria).then(function (data) {
      $scope.data = data;

      console.log(data.sql);
    });
  };

  $scope.fetchResult();

});