/*
  controller about client list on the mass edit item scope
 
*/

angularApp.controller('adminclientController', function($rootScope, $scope, $controller, api, $routeParams, $location, Restangular, $q) {

  $controller('CommonController',{$scope: $scope});

  $scope.filterCriteria = {
    pagesize: 7
  };
  
  $scope.clientgroup = '0';

  $scope.fetchResult = function () {
    
    $scope.filterCriteria.filterElement = 'clientgroup';    
    $scope.filterCriteria.filterElementValue = $scope.clientgroup;          
    
    api.getTable("client",$scope.filterCriteria).then(function (data) {
      $scope.data = data;

      $rootScope.clientList = data.items;

      console.log('adminclientController  : ' + data.sql);
    });
    
  };

  $scope.selectClientGroup = function(id) {

    if (id) {
      $scope.clientgroup = id;     
    } else {
      $scope.clientgroup = '0';
    }    

    $rootScope.$broadcast("ClientGroupSelected", {clientgroup: $scope.clientgroup });   
  }

  $scope.$on("AllClientChecked", function (event, args) {
    $scope.AllClientChecked = args.value;
  });

  /*get client groups */
  api.getTable("clientgroup", {}).then(function (data) {
      
      $scope.clientgroups = data.items;

      $rootScope.clientgroups = data.items;

  });

  /*get truck routes */
  api.getTable("truckroute", {}).then(function (data) {
      
      $rootScope.truckroutes = data.items;
  });

  $scope.fetchResult();
  

});