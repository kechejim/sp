angularApp.controller('editChangePriceController', function($rootScope, $scope, api, $routeParams, $location, Restangular) {

  $rootScope.page = 'Mass Edit Change Price';
  
  //init
  $scope.items = [];
  $scope.selectedClientGroup = '0';
  $rootScope.radioClient = ''; // temporary

  $scope.gridOptions = { 
    data: 'items',      
    showSelectionCheckbox: true,
    enableCellSelection: true,
    enableRowSelection: true,      
    selectedItems: [],
    multiSelect: true,    
    filterOptions: {filterText: '', useExternalFilter: false},
    showFilter: true,
    columnDefs: [
          {field:'code', displayName:'Code', enableCellEdit:false, width:'20%'},
          {field:'dsc', displayName: 'Desc', enableCellEdit: false, width:'40%'}, 
          {field:'tag', displayName:'Tag', enableCellEdit: false, width:'10%'},          
          {field:'unit', displayName:'Unit', enableCellEdit: false, width:'10%'},
          {field:'price', displayName:'Price', enableCellEdit: true, width:'20%'}          
        ]
  };

  $scope.selectClient = function(el) {
    if ($scope.selectedClientGroup == '0') {
      $rootScope.radioClient = el.id;

      var d = {
                clientgroup: '0',
                clientid: el.id
              };

      api.getItemsByClientGroupFromItemScope(d).then(function (data) {
        $scope.items = data.items; 
      });
    }

  };

  $scope.removeItems = function() {
    
    for (var i=0;i< $scope.gridOptions.selectedItems.length;i++ )
    {      
      var id = $scope.gridOptions.selectedItems[i].itemid;

      for (var j=0;j<$scope.items.length ;j++ )
      {
        if ($scope.items[j].itemid == id)
        {
          $scope.items.splice(j,1);          

          break;
        }
      }
    }
   
    var d = {
              clientgroup: $scope.selectedClientGroup,
              items:    $scope.gridOptions.selectedItems
            };

    if ($scope.selectedClientGroup == '0') {      
      d.clientid = $rootScope.radioClient;
    } else {
      d.clientid = '';
    }

    api.removeItemScope(d).then(function (data) {      
      alert('Removed Successfully!');
      
      $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length);   
    });
    
  };

  $scope.$on("ClientGroupSelected", function (event, args) {
    $scope.selectedClientGroup = args.clientgroup;
    
    if ($scope.selectedClientGroup == '0') {      
      $scope.items.splice(0, $scope.items.length);
      return;
    }

    var d = {
              clientgroup: args.clientgroup
            };

    api.getItemsByClientGroupFromItemScope(d).then(function (data) {
      $scope.items = data.items;
    });

  });
 
  $scope.save = function() {
  	    
    var d = {
              clientgroup: $scope.selectedClientGroup,    
              items: $scope.items
            };

    if ($scope.selectedClientGroup == '0') {      
      d.clientid = $rootScope.radioClient;
    } else {
      d.clientid = '';
    }

  	api.changeItemScopePrice(d).then(function (data) {
      
      alert('Changed Successfully!');

    });

  };

});
