angularApp.controller('mainController', function($rootScope, $scope, $controller, api, $routeParams, $location, Restangular, Auth) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Main';
  $rootScope.selectedTag = '';

  $scope.clientextrainfo = 1;

  $scope.$watch(Auth.isLoggedIn, function (value, oldValue) {

    if(!value && oldValue) {
      console.log("Disconnect");
      $location.path('/login');
    }

    if(value) {
      console.log("Connect");
      //Do something when the user is connected
    }

  }, true);

  $scope.$watch('clientextrainfo', function(n) {

    if ($rootScope.selectedClient)      
    {
      $scope.tAddress = $rootScope.selectedClient['address' + n];
      $scope.tContact = $rootScope.selectedClient['contact' + n];
      $scope.tMail    = $rootScope.selectedClient['email' + n];
      $scope.tTel     = $rootScope.selectedClient['tel' + n];           

      //get truck by client
      var criteria = { id: $rootScope.selectedClient['truck' + n] };
 
      api.getTruckRoute(criteria).then(function (data) {

        $rootScope.selectedTruck = data.item;    
 
      });
    }
  });

  $scope.fetchResult = function () {
     
    var criteria = { id: $scope.filterCriteria.id};
    api.getClientById(criteria).then(function (data) {
            
      if (data.item != '0') {

        $rootScope.selectedClient = data.item;
              
        //get truck by client
        var criteria = { id: $rootScope.selectedClient.truck1};
        api.getTruckRoute(criteria).then(function (data) {

          $rootScope.selectedTruck = data.item;           
        });
      }
      
    });

  };  

  $scope.addItem = function(i) {
    console.log(i);
    $rootScope.items.push({
      id: i.itemid,
      code: i.code,
      item: i.dsc,
      qty: 0,
      unit: i.unit,
      price: i.price,
      isNew: true,
      total: 0   
    });

    $rootScope.selectedTag = i.tag;

    $rootScope.$broadcast("tagChanged", {tag: i.tag });    

  };

});
