angularApp.controller('itemController', function($rootScope, $scope, $controller, api, $routeParams, Restangular) {

  $controller('CommonController',{$scope: $scope});

  $scope.countryStatus = [];

  $scope.filterCriteria = {
    pagesize: 8    
  };

  $scope.fetchResult = function () {
    
    if ($rootScope.selectedClient) {

      console.log('itemController :  fetchResult  tag' + $scope.filterCriteria.tag);

      $scope.filterCriteria.clientgroup = $rootScope.selectedClient.clientgroup;      
      $scope.filterCriteria.orderCond = 'frequency';
        
      api.getItemsByClientGroupFromItemScope($scope.filterCriteria).then(function (data) {

        $scope.data = data;    

      });
    }
  };

  $scope.fetchResult();

  $scope.$on("tagChanged", function (event, args) {
          
     $scope.filterCriteria.tag = args.tag;
     $scope.fetchResult();
  });

  $scope.changedTag = function() {
     
     $rootScope.selectedTag = $scope.selTag;
     $scope.filterCriteria.tag = $rootScope.selectedTag;
     $scope.fetchResult();
  };

  $scope.clickedCountry = function(id) {
    
    $scope.countryStatus[ id ] = !$scope.countryStatus[ id ];
        
    var k =0;
    for (var i=0;i <$scope.countries.length; i++) {
      if ($scope.countryStatus[ $scope.countries[i].id ] == true) {
        $scope.filterCriteria['country' + k] = $scope.countries[i].name;
        k++;
      }
    }    
    $scope.filterCriteria.countries = k;

    $scope.fetchResult();
  };

  api.getTable("tag",{}).then(function (data) {
      $scope.tags = data.items;      
  });

  api.getTable("country",{}).then(function (data) {
      $scope.countries = data.items; 

      for (var i=0;i <$scope.countries.length; i++) {
        $scope.countryStatus[ $scope.countries[i].id ] = false;
      }

  });

  

});