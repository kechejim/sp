
angularApp.controller('ModalInstanceCtrl', function ($scope, $modalInstance, comment) {

  $scope.comment = comment;
  
  $scope.ok = function () {
    $modalInstance.close('yes');
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('no');
  };
});

angularApp.controller('invoiceManageController', function($filter, $rootScope, $scope, $controller, $location, api, $routeParams, Restangular, $modal) {

  $controller('CommonController',{$scope: $scope});

  $rootScope.page = 'Invoice Management';

  api.getTable("truckroute", {}).then(function (data) {
      $scope.trucks = data.items;
  });

  $scope.$watch('remarksinfo', function(value) {
      
    switch (value) {
      case 'remarks1':
        $scope.tmpRemark = $scope.invoice.remarks.remarks1;
        break;
      case 'remarks2':
        $scope.tmpRemark = $scope.invoice.remarks.remarks2;
        break;
      case 'remarks3':
        $scope.tmpRemark = $scope.invoice.remarks.remarks3;
        break;       
    }
  });

  $scope.$watch('tmpRemark', function(value) {
    switch ($scope.remarksinfo) {
      case 'remarks1':
        $scope.invoice.remarks.remarks1 = value;
        break;
      case 'remarks2':
        $scope.invoice.remarks.remarks2 = value;
        break;
      case 'remarks3':
        $scope.invoice.remarks.remarks3 = value;
        break;        
    }

  });

  $rootScope.selectedClient = '';

  //configure invoice grid
  $scope.$on('ngGridEventEndCellEdit', function(evt){

      //console.log(evt.targetScope.row.entity);  
      
      var obj = evt.targetScope.row.entity;
      var sum = 0;

      for (var j=0;j< $scope.invoice.items.length;j++ )
      {
        sum += $scope.invoice.items[j].qty * $scope.invoice.items[j].price;  

        if (obj.id == $scope.invoice.items[j].id) {
            $scope.invoice.items[j].total = $scope.invoice.items[j].qty * $scope.invoice.items[j].price;
        };
      } 

      $scope.invoice.remarks.totalprice = sum;

      $scope.$apply();      
  });

  $scope.gridOptions = { 
    data: 'invoice.items',      
    enableCellSelection: true,
    enableRowSelection: true,      
    selectedItems: [],  
    showSelectionCheckbox: true,    
    multiSelect: true,
    columnDefs: [
          {field:'code', displayName:'Code', enableCellEdit:false, width:'10%'},
          {field:'item', displayName: 'Item', enableCellEdit: false, width:'30%'}, 
          {field:'qty', displayName:'Qty', enableCellEdit: true, width:'10%'},
          {field:'unit', displayName:'Unit', enableCellEdit: false, width:'10%'},
          {field:'price', displayName:'Price', enableCellEdit: true, width:'20%'},
          {field:'total', displayName:'Total', enableCellEdit: false, width:'20%'},
        ]
  };
  
  $scope.addItem = function(i) {
    $scope.invoice.items.push({
      itemid: i.itemid,
      code:i.code,   
      item: i.dsc,   
      qty: 0,
      unit: i.unit,
      price: i.price,      
      total: 0
    });   
  };

  $scope.removeItem = function() {

    for (var i=0;i< $scope.gridOptions.selectedItems.length;i++ )
    {
      console.log('selected   : ' + $scope.gridOptions.selectedItems[i].item);
      var id = $scope.gridOptions.selectedItems[i].id;

      for (var j=0;j<$scope.invoice.items.length ;j++ )
      {
        if ($scope.invoice.items[j].id == id)
        {
          $scope.invoice.items.splice(j,1);

          break;
        }
      }
    }

    $scope.gridOptions.selectedItems.splice(0,$scope.gridOptions.selectedItems.length); 

  };

  $scope.filterCriteria = {
    page: 1,
    pagesize: 8
  };

  $scope.fetchResult = function () {
       
    api.getAllInvoices($scope.filterCriteria).then(function (data) {
      
      $scope.invoices = data;

    });
  };    
  $scope.fetchResult();

  $scope.selectInvoice = function(item) {
        
    var d = {
      invoiceno: item.invoiceno
    };
    $scope.selectedInvoiceNo = item.invoiceno;

    api.getInvoiceContent(d).then(function(data) {

      $scope.invoice = data;     
      $rootScope.invoicePreview = data;      
      $rootScope.invoiceNumber  = $scope.selectedInvoiceNo;

      $rootScope.selectedTag = data.items[0].tag;      
    });

    var criteria = { id: item.clientid};
    api.getClientById(criteria).then(function (data) {

      if (data.item != '0') {

        $rootScope.selectedClient = data.item; 
        $rootScope.$broadcast("tagChanged", {tag: $rootScope.selectedTag });       
      }      
    });

  };
  
  //
  // confirm dialog for update/preview 
  //
  $scope.update = function() {

    var modalInstance = $modal.open({
      templateUrl: 'updateConfirm.html',
      controller: 'ModalInstanceCtrl',
      size: 'sm',
      resolve: {
        comment: function () {
          return 'Are you going to update changement of this invocie ?';
        }
      }
    });
    
    modalInstance.result.then(function (selectedbtn) {
      //yes
      if ($scope.invoice) {

        var invoice = {
          'invoiceno':$scope.selectedInvoiceNo,
          'truckid' : $scope.invoice.truckid,
          'clientid': $scope.invoice.clientid,
          'items'   : $scope.invoice.items,
          'remarks1': $scope.invoice.remarks.remarks1,
          'remarks2': $scope.invoice.remarks.remarks2,
          'remarks3': $scope.invoice.remarks.remarks3,
          'cashcredit' : $scope.invoice.remarks.cashcredit,
          'total'   : $scope.invoice.remarks.totalprice,
          'issuedDate': $scope.invoice.remarks.issuedDate
        };

        $rootScope.lastChangedInvoiceNum = $scope.selectedInvoiceNo;

        api.updateInvoice(invoice).then(function (data) {
          
          $scope.Message = 'Invoice is updateed on '+ data + ' successfully !' ;
          //$('#mini-notification').miniNotification({closeButton: false});             
          
          alert($scope.Message);
        });

      }

    }, function () {
      //no
    });
  
  };

  $scope.preview = function() {
    
    var modalInstance = $modal.open({
      templateUrl: 'updateConfirm.html',
      controller: 'ModalInstanceCtrl',
      size: 'sm',
      resolve: {
        comment: function () {
          return 'Are you going to print this invocie ?';
        }
      }
    });
    
    modalInstance.result.then(function (selectedbtn) {
      
      $location.path('/preview');

    }, function() {

    });

  };

});