
var angularApp = angular.module('angularApp', ['ngRoute','ngCookies','$strap.directives','restangular','ui.bootstrap.pagination','ui.bootstrap.tooltip', 'ngGrid', 'ngSanitize', 'ngAnimate','ui.bootstrap','dialogs', 'ngTable']);

// Routes
angularApp.config(['$routeProvider',
  function($routeProvider) {
	$routeProvider.
		when('/login', {
		  templateUrl: 'public/partials/login.html',
		  controller: 'loginController'
		}).
		when('/main', {
		  templateUrl: 'public/partials/main.html',
		  controller: 'mainController'
		}).
		when('/customer', {
		  templateUrl: 'public/partials/customer.html',
		  controller: 'clientController'
		}).
		when('/truck', {
		  templateUrl: 'public/partials/truck.html',
		  controller: 'truckController'
		}).
		when('/invoices', {
		  templateUrl: 'public/partials/invoicereview.html',
		  controller: 'invoiceManageController'
		}).
		when('/preview', {
		  templateUrl: 'public/partials/preview.html',
		  controller: 'previewController'
		}).
		when('/daily', {
		  templateUrl: 'public/partials/daily.html',
		  controller: 'summaryController'
		}).
		when('/admin/edit_item_scope', {
		  templateUrl: 'public/partials/admin/edit_item_scope.html',
		  controller: 'editItemScopeController'
		}).
		when('/admin/edit_change_price', {
		  templateUrl: 'public/partials/admin/edit_change_price.html',
		  controller: 'editChangePriceController'
		}).
		when('/admin/edit_clients', {
		  templateUrl: 'public/partials/admin/edit_clients.html',
		  controller: 'editClientController'
		}).
		when('/admin/edit_clientgroups', {
		  templateUrl: 'public/partials/admin/edit_clientgroup.html',
		  controller: 'editClientGroupController'
		}).
		when('/admin/edit_item', {
		  templateUrl: 'public/partials/admin/edit_items.html',
		  controller: 'editItemController'
		}).
		otherwise({
		  redirectTo: '/customer'
		});		
	
  }
]);

angularApp.config(
  function(RestangularProvider) {
    RestangularProvider.setBaseUrl('api/api.php');
    RestangularProvider.setDefaultHttpFields({cache: true});
  }
);

//  Global Initialization

angularApp.run(function($rootScope, $location, $http, $cookieStore, Restangular, Auth, keyboardManager) {

   $rootScope.items = [];
   
   //preview draft of printed invoice
   $rootScope.invoicePreview = {};
   $rootScope.invoiceNumber = "";

   $rootScope.lastChangedInvoiceNum = '';
   
   /*page tab*/
   $rootScope.page = 'Main';   

   $rootScope.isAdmin = false;
   $rootScope.username = '';

   //
   // check if user is logged in 
   //   
   $rootScope.$on('$routeChangeStart', function (event) {

        if (!Auth.isLoggedIn()) {
            console.log('DENY');
            event.preventDefault();

            $location.path('/login');
        }
        else {
            console.log('ALLOW');

			$rootScope.isAdmin = Auth.getUser().isAdmin;
			$rootScope.username = Auth.getUser().name;
            
        }
    });

    //
    // logout
    //
    $rootScope.logout = function() {
    	
    	Auth.removeUser();
		
		$rootScope.isAdmin = false;
   		$rootScope.username = '';

		$location.path('/login');

    };

    //
    // Shorcut key - Event
    //
  	keyboardManager.bind('ctrl+h', function() {
		$location.path('/customer');
	});

	keyboardManager.bind('ctrl+i', function() {
		$location.path('/invoices');
	});

	keyboardManager.bind('ctrl+d', function() {
		$location.path('/daily');
	});

	keyboardManager.bind('ctrl+s', function() {
		if ($rootScope.page == 'Main') {			
			$rootScope.$broadcast("SaveFromSortkey", {});    
		}		
	});

	keyboardManager.bind('ctrl+shift+s', function() {
		$location.path('/admin/edit_item_scope');
	});

	keyboardManager.bind('ctrl+shift+p', function() {
		$location.path('/admin/edit_change_price');
	});

	keyboardManager.bind('ctrl+shift+c', function() {
		$location.path('/admin/edit_clients');
	});

	keyboardManager.bind('ctrl+shift+g', function() {
		$location.path('/admin/edit_clientgroups');
	});	

	keyboardManager.bind('ctrl+shift+i', function() {
		$location.path('/admin/edit_item');
	});	

});
