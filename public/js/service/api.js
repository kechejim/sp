//  API section for getting data
//
//  We are using Restangular here, the code bellow will just make an ajax request to /api/states & /api/customers?{filtercriteria}
//  prepend /api before making any request with restangular

angularApp.factory('api', function(Restangular, $q) {
  
  //
  // Common
  //
  var dataset,
  getRequest = function (func, data) {
    var deferred = $q.defer();

    if(!dataset || true) {
      Restangular.all(func).getList(data).then(function(data) {

        dataset = data;
        dataset.headers = [];
        if(data.itemcount > 0) {
          for(var key in data.items[0]) {
            dataset.headers.push(key);
          };
        }
        deferred.resolve(dataset);
      }, function () {
        console.log("ERROR");
      });
    } else {
      deferred.resolve(dataset);
    }
    return deferred.promise;
  },
  postRequest = function(func, data) {
    var deferred = $q.defer();
    if(!dataset || true) {
      Restangular.all(func).post(data).then(function(data) {
        dataset = data;
        dataset.headers = [];
        if(data.itemcount > 0) {
          for(var key in data.items[0]) {
          dataset.headers.push(key);
          };
        }
        deferred.resolve(dataset);

      }, function() {
        console.log('Error');
      });
    } else {
      deferred.resolve(dataset);
    }
       
    return deferred.promise;
  };

  //
  // rest api interface
  //
  var getTable = function(table,options) {
    return getRequest (table, options); 
  },
  login = function(data) {
    return postRequest("login", data);
  },
  getClientById = function(options) {
    return postRequest("getclientbyid", options);
  },
  getTruckRoute = function(options) {
    return postRequest("gettruckroute", options);
  },
  getDailySummary = function(options) {    
    return getRequest ("getdailysummary", options);     
  },
  saveInvoice = function(data) {
    return postRequest("register_invoice", data);
  },
  updateInvoice = function(data) {
    return postRequest("update_invoice", data);
  },
  saveItemScope = function(data) {
    return postRequest("saveitemscope", data);    
  }, 
  getAllInvoices = function(options) {
    return getRequest ("get_all_invoices", options);    
  },
  getInvoiceContent = function(options) {
    return getRequest ("getinvoicecontent", options);
  },
  getClientFromItemScope = function(options) {
    return getRequest ("getclientfromitemscope", options);    
  },
  getItemsByClientGroupFromItemScope = function(options) {
    return getRequest ("getitemsbyclientgroupfromitemscope", options);    
  },
  changeItemScopePrice = function(data) {
    return postRequest("changeitemscopeprice", data);    
  },
  removeItemScope = function(data) {
    return postRequest("removeitemscope", data);        
  },
  removeItems = function(data) {
    return postRequest("removeitems", data);            
  },
  saveItems = function(data) {
    return postRequest("saveitems", data);   
  },
  updateItems = function(data) {
    return postRequest("updateitems", data);
  },
  createClient = function(data) {
    return postRequest("createclient", data);    
  },
  updateClient = function(data) {
    return postRequest("updateclient", data);
  },
  createClientGroup = function(data) {
    return postRequest("createclientgroup", data);
  },
  updateClientGroup = function(data) {
    return postRequest("updateclientgroup", data);    
  },
  deleteClientGroup = function(data) {
    return postRequest("deleteclientgroup", data);    
  };

  return {
    login     : login,
    getTable  : getTable,

    saveInvoice       : saveInvoice,
    updateInvoice     : updateInvoice,
    getInvoiceContent : getInvoiceContent,    
    getAllInvoices    : getAllInvoices,        

    getTruckRoute     : getTruckRoute,   

    saveItemScope     : saveItemScope,
    getClientFromItemScope : getClientFromItemScope,
    getItemsByClientGroupFromItemScope : getItemsByClientGroupFromItemScope,
    changeItemScopePrice : changeItemScopePrice,
    removeItemScope   : removeItemScope,

    getClientById     : getClientById,
    getDailySummary   : getDailySummary,

    removeItems       : removeItems,
    saveItems         : saveItems,
    updateItems       : updateItems,

    createClient: createClient,    
    updateClient: updateClient,

    createClientGroup: createClientGroup,
    updateClientGroup: updateClientGroup,
    deleteClientGroup: deleteClientGroup
  };

});

