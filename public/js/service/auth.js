angularApp.factory('Auth', function(Restangular, $q, $cookieStore) {

  return {
    setUser : function(aUser){
        
        $cookieStore.put('user', aUser);
    },
    removeUser : function() {
        
        $cookieStore.remove('user');  
    },
    getUser : function(){

        var user = $cookieStore.get('user');

    	return user;
    },
    isLoggedIn : function(){
        var user = $cookieStore.get('user');
        return(user)? user : false;        
    }
  }
  
});

