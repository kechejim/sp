<?php

/************************************************
*  												*
* 		 Create/Update client group	 			*
*												*
*************************************************/
//create client group
$app->post('/createclientgroup', function () use($app) {
	
	$where = "";
		
	$paramValue = json_decode($app->request()->getBody());
	$clientgroup = $paramValue->clientgroup;


	$sql = "INSERT INTO clientgroup ";

	$columns = '( name )';
	$values =  '("'. $clientgroup->name.'")';

	$sql .= $columns.' VALUES '.$values;
	
	R::exec($sql);

	$ret = 'Success';
	echo $ret;
});

//update client group
$app->post('/updateclientgroup', function () use($app) {
			
	$paramValue = json_decode($app->request()->getBody());
	$clientgroup = $paramValue->clientgroup;

	$sql  = 'UPDATE clientgroup SET name = "'.$clientgroup->name.'"';	
	$sql .= ' WHERE id ='.$clientgroup->id;
	
	R::exec($sql);
	$ret = 'Success';

	echo $ret;
});

//delete client group
$app->post('/deleteclientgroup', function () use($app) {
			
	$paramValue = json_decode($app->request()->getBody());
	$clientgroup = $paramValue->clientgroup;

	$sql  = 'DELETE FROM clientgroup WHERE';	
	$sql .= ' id ='.$clientgroup->id;
	
	R::exec($sql);
	$ret = 'Success';

	echo $ret;
});

?>