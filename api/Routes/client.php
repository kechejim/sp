<?php

//search client by entering client id or name
$app->post('/getclientbyid', function () use($app) {
	
	$where = "";
		
	$paramValue = json_decode($app->request()->getBody());
	$keyword = $paramValue->id;
	//
	//	Page Size
	//	
	$sql  = "SELECT * ";	
	$sql .= "From client ";
	$sql .= "WHERE code like '%".$keyword ."%' OR name like '%".$keyword."%'" ;
		 
	$all = R::getAll ($sql);
		
	if (sizeof($all) == 0) {
		$data['item']=0;
		echo json_encode($data);
		exit;
	}
	
	//
	//	Stuff data here
	//	
	$data['item']	= $all[0];
		
	echo json_encode($data);
});

/************************************************
*  												*
* 		 Create/Update client 		 			*
*												*
*************************************************/
//create client
$app->post('/createclient', function () use($app) {
	
	$where = "";
		
	$paramValue = json_decode($app->request()->getBody());
	$client = $paramValue->client;


	$sql = "INSERT INTO client ";

	$columns = '(';
	$values = '(';

	$i = 0;
	foreach ($client as $key => $value) {
		if ($i != 0) {
			$columns .= ',';
			$values .= ',';
		}
		$columns .= $key;
		$values .= '"'.$value.'"';
		
		$i++;
	}

	$columns .= ')';
	$values  .= ')';

	$sql .= $columns.' VALUES '.$values;
	
	R::exec($sql);

	$ret = 'Success';
	echo $ret;
});

//update client
$app->post('/updateclient', function () use($app) {
	
	$where = "";
		
	$paramValue = json_decode($app->request()->getBody());
	$client = $paramValue->client;

	$sql  = 'UPDATE client SET ';

	$i = 0;
	foreach ($client as $key => $value) {
		if ($i != 0) {
			$sql .= ',';
		}
		$sql.= $key.'="'.$value.'"';
		$i++;
	}
	$sql .= ' WHERE id ='.$client->id;
	
	R::exec($sql);
	$ret = 'Success';

	echo $ret;
});

/************************************************
*  												*
* 		 mass edit item scope / price 			*
*												*
*************************************************/
//save mapping of clientgroup:client:item scope
$app->post('/saveitemscope', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	R::begin();

	if ($paramValue->clientgroup != '0') {
		//Remove previous item list about client group
		$sql = 'DELETE FROM itemscope WHERE clientgroup = '.$paramValue->clientgroup;
	} else {
		//Remove previous item list about client list (for individual)
		$sql = 'DELETE FROM itemscope WHERE clientid IN (';
		for ($i = 0; $i <  count($paramValue->clientList); $i++) {
			if ($i !=0 ) {
				$sql .= ',';
			}
			$sql .= $paramValue->clientList[$i]->id;
		}
		$sql .= ')';	
	}
	R::exec($sql);
	
	
	//
	//saving items by client
	//
	$sql = "INSERT INTO itemscope (clientgroup, itemid, price, clientid) VALUES ";

	$k = 0;	
	for ($j= 0; $j < count($paramValue->clientList) ; $j++) { 

		for ($i= 0; $i < count($paramValue->itemList) ; $i++) { 
			$itemid = $paramValue->itemList[$i]->id;
			$price	= $paramValue->itemList[$i]->price;

			if ($k != 0) {
				$sql .= ",";				
			}

			$sql .= '('.$paramValue->clientgroup.','.$itemid.','.$price.','.$paramValue->clientList[$j]->id.')';		
			$k = 1;
		}		
	}
	R::exec($sql);

	R::commit();
	
	$ret = 'Success';
	
	echo $ret;
});

//get item list by clientgroup from mass edit item scope
$app->get('/getitemsbyclientgroupfromitemscope', function () use ($app) {
			
	$pagesize = 64000;
	if (isset($_REQUEST['pagesize']))
	{
		$pagesize=$_REQUEST['pagesize'];
	}

	$start=0;
	$currentPage=1;
	$extra = "";
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body); 
	if($request->get('page')) {
		$currentPage = $request->get('page');
		$start = ($pagesize * ($currentPage -1));
	}

	//
	//	Page Size
	//
	$limit = ' limit ' . $start . ',' . $pagesize;
	if (isset($_REQUEST['export']))
	{
		$limit = " limit 64000";
	}
	
	if (isset($_REQUEST['item'])) { 
		if ($_REQUEST['item'] != '') {
			$extra = " AND ( ie.code like '%".$_REQUEST['item']."%'";
			$extra.= " OR ie.desc like '%".$_REQUEST['item']."%' ) ";
		}
	};

	if (isset($_REQUEST['clientid'])) { 
		if ($_REQUEST['clientid'] != '') {
			$extra = " AND it.clientid = '".$_REQUEST['clientid']."'";			
		}
	};

	if (isset($_REQUEST['tag'])) { 
		$extra = " AND ie.tag like '%".$_REQUEST['tag']."%'";		
	};

	if (isset($_REQUEST['countries'])) { 
		if ($request->get('countries') > 0) {
			$extra .= ' AND (';
			for ($i = 0; $i <  $request->get('countries'); $i++) {			

				if ($i != 0) {
					$extra .= ' OR ';
				}
				$extra .= " ie.country = '".$request->get('country'.$i)."' ";

			}
			$extra .= ') ';
		}		
	};
	$clientgroup = $request->get('clientgroup');

	$statsql  = 'SELECT it.itemid as itemid ';	
	$statsql .= 'From itemscope it ';
	$statsql .= 'INNER JOIN item ie ';
	$statsql .= 'on ie.id = it.itemid ';	
	$statsql .= 'WHERE it.clientgroup ='.$clientgroup;
	$statsql .= $extra;

	$sql  = 'SELECT it.itemid as itemid, ie.code as code, ie.desc as dsc, ie.unit as unit, it.price as price, ie.tag as tag, it.clientid as clientid ';	
	$sql .= 'From itemscope it ';
	$sql .= 'INNER JOIN item ie ';
	$sql .= 'on ie.id = it.itemid ';	
	$sql .= 'WHERE it.clientgroup ='.$clientgroup;
	$sql .= $extra;
	$sql .= ' ORDER BY ie.frequency DESC ';
	$sql .= $limit;

	$stat = R::getAll ($statsql);
	$all  = R::getAll ($sql);

	if (sizeof($all) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}

	//
	//	Paging columns here
	//	
	$data['itemcount']	= sizeof($all);
	$data['currentpage']= $currentPage*1;
	$data['totalitems']	= sizeof($stat);
	$data['totalpages']	= ceil(sizeof($stat) / $pagesize);

	//
	//	Stuff data here
	//
	$data['items']= $all;
		
	echo json_encode($data);
});

//update item price by clientgroup for mass edit item scope
$app->post('/changeitemscopeprice', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	$clientgroup = $paramValue->clientgroup;
	$clientid 	= $paramValue->clientid;

	if ($clientid != '') {

		$sql = "UPDATE itemscope SET price = CASE";

		for ($i = 0; $i <  count($paramValue->items); $i++) {

			$sql .= ' WHEN itemid ='.$paramValue->items[$i]->itemid.' AND clientid ='.$clientid.' THEN ';
			$sql .= $paramValue->items[$i]->price;		
		}

		$sql .= ' END WHERE clientgroup IN ('.$clientgroup.')';
	} else {
		$sql = "UPDATE itemscope SET price = CASE";

		for ($i = 0; $i <  count($paramValue->items); $i++) {

			$sql .= ' WHEN itemid ='.$paramValue->items[$i]->itemid.' THEN ';
			$sql .= $paramValue->items[$i]->price;		
		}

		$sql .= ' END WHERE clientgroup IN ('.$clientgroup.')';
	}

	R::exec($sql);
	
	$ret = 'Success';
	
	echo $ret;
});

//delete items by client from item scope
$app->post('/removeitemscope', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	$clientgroup= $paramValue->clientgroup;
	$clientid 	= $paramValue->clientid;

	if ($clientid != '') {
		$sql = 'DELETE FROM itemscope WHERE (clientgroup, itemid, clientid) IN (';
		for ($i = 0; $i <  count($paramValue->items); $i++) {
			if ($i != 0) {
				$sql .= ',';
			}
			$sql .= '('.$clientgroup.','.$paramValue->items[$i]->itemid.','.$clientid.')';
		}
	} else {
		$sql = 'DELETE FROM itemscope WHERE (clientgroup, itemid) IN (';
		for ($i = 0; $i <  count($paramValue->items); $i++) {
			if ($i != 0) {
				$sql .= ',';
			}
			$sql .= '('.$clientgroup.','. $paramValue->items[$i]->itemid .')';
		}		
	}
	$sql .= ')';

	R::begin();	
	R::exec($sql);	
	R::commit();
		
	$ret = 'Success';
	
	echo $ret;
});

?>