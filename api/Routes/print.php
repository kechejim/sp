<?php

$app->get('/print/:pc/:invoice', function($pc,$invoiceno) use ($app){
	//
	//	Create spool directory per PC if not exists
	//	
	//	PC monitors for queued directory for invoice print.  After printing, PC moces it to the done directory
	//
	$spooldir = '../spool/'.$pc;
	if (!file_exists($spooldir)) {
	    mkdir($spooldir, 0777, true);
	    mkdir($spooldir."/queued", 0777, true);
	    mkdir($spooldir."/done", 0777, true);
	}
	//
	//	Code to output real invoice to text file here
	//
	$newfile = $spooldir . "/" . $invoiceno . '.txt';
	$spoolfile = $spooldir . "/queued/" . $invoiceno . '.txt';
	$donefile = $spooldir . "/done/" . $invoiceno . '.txt';
	//
	//	If invoice already printed, move back to queue for reprint
	//	If invoice already in spool file, ignore
	//	else create invoice print file
	//
	if (file_exists($donefile)) {
		//echo "reprinting\n";
		//rename ($donefile,$spoolfile);
		echo "deleting\n";
		unlink ($donefile);
	}
	if (!file_exists($spoolfile)){
		echo "creating\n";
		//
		//	Start Change Here
		//
	    $sql = "select * from invoice join item on invoice.itemid = item.id where invoiceno = '" . $invoiceno . "'";
	    $ret = R::getAll($sql);
	    $contents = $sql;
	    $contents .= "\n";
	    $contents .= json_encode($ret);

		file_put_contents($newfile, $contents);
		//
		//	End Change Here
		//
		//
		//	Move file to queue directory only after finished writing
		//	to prevent PC printing half an invoice during write
		//
		echo "moving\n";
		rename ($newfile,$spoolfile);
	} else {
		echo "already printing\n";
	}
   	echo "<table border=1><tr><td>".$pc."</td><td>".$invoiceno."</td><td>test</td><td>test</td></tr></table>";
});

?>