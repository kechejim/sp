<?php

//
//	Generic table with paging
//

$app->get('/:table', function ($table) use ($app) {

	//
	//	Handle paging
	//
	$pagesize = 64000;
	if (isset($_REQUEST['pagesize'])) { 
		$pagesize=$_REQUEST['pagesize'];	
	};

	$start=0;
	$currentPage=1;
	$where = "";
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body); 
	if($request->get('page')) {
		$currentPage = $request->get('page');
		$start = ($pagesize * ($currentPage -1));
	}

	//
	//	Fields selection
	//
	$fields = "*";

	$filterElement = '';
	$filterElementValue = '';

	if ($request->get('filterElement')) {
		$filterElement = $_REQUEST['filterElement'];
		$filterElementValue = $_REQUEST['filterElementValue'];
	}
	//
	// Filter conditions here
	//
	if ($filterElement != '') { 
		$where = addWhere($where, "`".$filterElement."` like '%".$filterElementValue."%'");
	};

	if (isset($_REQUEST['tag'])) { 
		$where = addWhere($where, "`tag` like '%".$_REQUEST['tag']."%'");		
	};

	if (isset($_REQUEST['item'])) { 
		$where = addWhere($where, "( `code` like '%".$_REQUEST['item']."%'");
		$where = addWhereOr($where, "`desc` like '%".$_REQUEST['item']."%' )");
	};

	if (isset($_REQUEST['client'])) { 
		$where = addWhere($where, "( `name` like '%".$_REQUEST['client']."%'");
		$where = addWhereOr($where, "`code` like '%".$_REQUEST['client']."%' )");
	};

	//in the case of clientgroup table
	if (isset($_REQUEST['name'])) { 
		$where = addWhere($where, "`name` like '%".$_REQUEST['name']."%'");		
	};
	
	//in the case of item table
	if (isset($_REQUEST['hidden'])) { 
		$where = addWhere($where, " `hidden` = 0 ");		
	};
	
	//
	//	Page Size
	//
	$limit = ' limit ' . $start . ',' . $pagesize;
	if (isset($_REQUEST['export']))
	{
		$limit = " limit 64000";
	}


	//
	//	Summary columns here
	//
	$summcols = "";

	$statsql = 'select count(*) as count ' . $summcols . ' from ' . $table . ' ' . $where;
	$sql = 	'select ' . $fields . ' from ' . $table . ' ' . $where;

	if($request->get('orderCond')) {
		$order = $request->get('orderCond');
		$sql .= 'ORDER BY '. $order . ' desc ';
	}
	$sql .= $limit;

	$stat = R::getAll ($statsql);

	if (sizeof($stat) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}
	$all = R::getAll ($sql);

	//
	//	Paging columns here
	//	
	$data['itemcount']=sizeof($all);
	$data['currentpage']=$currentPage*1;
	$data['totalitems']=$stat[0]['count'];
	$data['totalpages']=ceil($stat[0]['count'] / $pagesize);

	//
	//	Stuff data here
	//
	$data['items']=$all;
	$data['sql'] = $sql;
	//
	//	Export code here
	//
	if (isset($_REQUEST['export']))
	{
		download_send_headers($table . "_export_" . date("Y-m-d") . ".csv");
		echo array2csv($data['items']);
	} else {
		$app->response()->header('Content-Type', 'application/json');
		if($table=="users"){
			if($_SESSION['level'] == 2){
				echo json_encode($data);
			}else{
				echo "errorprivileges";
			}
		}else{
			echo json_encode($data);
		}
	}
});

?>