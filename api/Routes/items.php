<?php
//daily summary
$app->get('/getdailysummary', function () use($app) {
		
	$pagesize=$_REQUEST['pagesize'];
	$start=0;
	$currentPage=1;
	$extra = "";
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body); 
	if($request->get('page')) {
		$currentPage = $request->get('page');
		$start = ($pagesize * ($currentPage -1));
	}

	$issuedDate = $request->get('date');	
	//
	//	Page Size
	//
	$limit = ' limit ' . $start . ',' . $pagesize;
	if (isset($_REQUEST['export']))
	{
		$limit = " limit 64000";
	}
	
	if (isset($_REQUEST['code'])) { 
		if ($_REQUEST['code'] != '') {
			$extra = " AND ( it.code like '%".$_REQUEST['code']."%'";
			$extra.= " OR it.desc like '%".$_REQUEST['code']."%' ) ";
		}
	};

	$statsql  = 'SELECT iv.itemid as itemid ';
	$statsql .= 'FROM invoice as iv ';
	$statsql .= 'INNER JOIN invoice_remarks ir ';
	$statsql .= 'on ir.invoiceno = iv.invoiceno ';
	$statsql .= 'and ir.issuedDate = "'. $issuedDate .'" ';
	$statsql .= 'INNER JOIN item it ';
	$statsql .= 'on it.id = iv.itemid ';
	$statsql .= $extra;
	$statsql .= 'GROUP BY iv.itemid ';		

	$sql  = 'SELECT iv.itemid as itemid, it.code as itemcode, it.desc as itemdesc, it.price as itemprice, it.unit as itemunit, SUM(iv.itemqty) as totalqty, SUM(iv.itemqty)*it.price as totalprice ';
	$sql .= 'FROM invoice as iv ';
	$sql .= 'INNER JOIN invoice_remarks ir ';
	$sql .= 'on ir.invoiceno = iv.invoiceno ';

	if ($issuedDate != '') {
		$sql .= 'and ir.issuedDate = "'. $issuedDate .'" ';
	}
	
	$sql .= 'INNER JOIN item it ';
	$sql .= 'on it.id = iv.itemid ';
	$sql .= $extra;
	$sql .= 'GROUP BY iv.itemid ';		
	$sql .= $limit;
	
	$stat = R::getAll ($statsql);
		
	$all = R::getAll ($sql);
	
	if (sizeof($all) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}

	//
	//	Paging columns here
	//	
	$data['itemcount']	= sizeof($all);
	$data['currentpage']= $currentPage*1;
	$data['totalitems']	= sizeof($stat);
	$data['totalpages']	= ceil(sizeof($stat) / $pagesize);

	//
	//	Stuff data here
	//
	$data['items']= $all;
		
	echo json_encode($data);
});

//remove items from item , itemscope
$app->post('/removeitems', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	//Remove from itemscope

	$sql1 = 'DELETE FROM itemscope WHERE itemid IN (';
	for ($i = 0; $i <  count($paramValue->items); $i++) {
		if ($i !=0 ) {
			$sql1 .= ',';
		}
		$sql1 .= $paramValue->items[$i]->id;
	}
	$sql1 .= ')';	
	
	//hidden removed item from item
	$sql2  = 'UPDATE item SET hidden=1 ';
	$sql2 .= ' WHERE id IN (';
	for ($i = 0; $i <  count($paramValue->items); $i++) {
		if ($i !=0 ) {
			$sql2 .= ',';
		}
		$sql2 .= $paramValue->items[$i]->id;
	}
	$sql2 .= ')';

	R::begin();
	
	R::exec($sql1);
	R::exec($sql2);

	R::commit();

	$ret = 'success';

	echo $ret;	
});

//save items
$app->post('/saveitems', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	$sql = "INSERT INTO item (`code`, `desc`, `unit` , `price`, `tag`, `country`) VALUES ";
	
	$k = 0;
	for ($i = 0; $i <  count($paramValue->items); $i++) {
		
		if ($paramValue->items[$i]->code == '') {
			continue;
		}

		if ($k != 0) {
			$sql .= ',';
		}

		$sql .= "('".$paramValue->items[$i]->code."','".$paramValue->items[$i]->desc;
		$sql .= "','".$paramValue->items[$i]->unit."',".$paramValue->items[$i]->price;
		$sql .= ",'".$paramValue->items[$i]->tag."')";	
		$k++;
	}

	R::begin();
	R::exec($sql);
	R::commit();

	$ret = 'Success';

	echo $ret;
});

//update items
$app->post('/updateitems', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	$sql = "INSERT INTO item (`id`, `code`, `desc`, `unit`, `price`, `tag`, `country`) VALUES ";
	
	for ($i = 0; $i <  count($paramValue->items); $i++) {
		
		if ($i != 0) {
			$sql .= ',';
		}

		$sql .= "(".$paramValue->items[$i]->id.",'".$paramValue->items[$i]->code."','";
		$sql .= $paramValue->items[$i]->desc."','".$paramValue->items[$i]->unit."',";
		$sql .= $paramValue->items[$i]->price.",'".$paramValue->items[$i]->tag."','";
		$sql .= $paramValue->items[$i]->country."')";
	}
	
	$sql .= " ON DUPLICATE KEY UPDATE `code`=VALUES(`code`), `desc`=VALUES(`desc`), `unit`=VALUES(`unit`), `price`=VALUES(`price`), `tag`= VALUES(`tag`), `country`= VALUES(`country`) ";

	R::begin();
	R::exec($sql);
	R::commit();

	$ret = 'Success';

	echo $ret;
});

?>