<?php

//get invoice data by invoice number
$app->get('/getinvoicecontent', function () use($app) {
	
	$where = "";
	$request = $app->request();	
	$invoiceno = $request->get('invoiceno');

	//
	//	Page Size
	//	
	$sql  = 'SELECT distinct it.code as code ';
	$sql .=	', it.desc as item ';
	$sql .=	', it.id as itemid ';
	$sql .=	', i.itemqty as qty ';
	$sql .= ', it.price as price ';
	$sql .= ', it.unit as unit ';	
	$sql .= ', it.unit as unit ';	
	$sql .= ', i.itemqty*it.price as total ';
	$sql .=	', i.truckid as truckid ';
	$sql .=	', i.clientid as clientid ';
	$sql .=	', it.tag as tag ';
	$sql .= ', ct.code as customercode ';
	$sql .= ', ct.name as customername ';	
	$sql .= ', ct.address1 as customeraddress ';	
	$sql .= ', ct.contact1 as customercontact ';	
	$sql .= ', ct.tel1 as customertel ';	
	$sql .= ', tr.desc as truckdesc ';
	$sql .= 'From invoice i ';
	$sql .= 'INNER JOIN item it ';	
	$sql .= 'on i.itemid = it.id ';
	$sql .= 'INNER JOIN client ct ';
	$sql .= 'on i.clientid = ct.id ';	
	$sql .= 'INNER JOIN truckroute tr ';
	$sql .= 'on i.truckid = tr.id ';
	$sql .= 'WHERE i.invoiceno = "'.$invoiceno.'"';

	$all = R::getAll ($sql);
	
	if (sizeof($all) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}
	
	//
	//	Stuff data here
	//
	$data['items']			=	$all;
	$data['customercode']	=	$all[0]['customercode'];
	$data['customername']	=	$all[0]['customername'];
	$data['customeraddress']=	$all[0]['customeraddress'];
	$data['customercontact']=	$all[0]['customercontact'];
	$data['customertel']	=	$all[0]['customertel'];
	$data['truckdesc']		= 	$all[0]['truckdesc'];
	$data['truckid']		= 	$all[0]['truckid'];
	$data['clientid']		= 	$all[0]['clientid'];

	//getting remarks
	$sql  = 'SELECT remarks1, remarks2, remarks3, cashcredit, issuedDate, totalprice, lastupdated ';	
	$sql .= 'From invoice_remarks ';	
	$sql .= 'WHERE invoiceno = "'.$invoiceno.'"';
	
	$all = R::getAll ($sql);

	if (sizeof($all) == 0) {
		$data['remarks']=0;		
	} else {
		$data['remarks'] = $all[0];
	}

	echo json_encode($data);
});

//get all invoices by page
$app->get('/get_all_invoices', function () use ($app) {

	//reference tables : invoice, truckroute, client

	$pagesize=$_REQUEST['pagesize'];
	$start=0;
	$currentPage=1;
	$where = "";
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body); 
	if($request->get('page')) {
		$currentPage = $request->get('page');
		$start = ($pagesize * ($currentPage -1));
	}

	if (isset($_REQUEST['invoiceno'])) { 
		$where = addWhereOr($where, "i.invoiceno like '%".$_REQUEST['invoiceno']."%' ");		
	};

	//
	//	Page Size
	//
	$limit = ' limit ' . $start . ',' . $pagesize;
	if (isset($_REQUEST['export']))
	{
		$limit = " limit 64000";
	}
	
	$statsql  =  'SELECT count(distinct i.invoiceno) as count ';
	$statsql .= 'From invoice i ';
	$statsql .= 'INNER JOIN truckroute t ';
	$statsql .= 'on i.truckid = t.id ';
	$statsql .= 'INNER JOIN client c ';
	$statsql .= 'on i.clientid = c.id ';
	$statsql .= $where;

	$sql  = 'SELECT distinct i.invoiceno as invoiceno ';
	$sql .=	', t.desc as truckdesc ';
	$sql .= ', c.name as clientname ';
	$sql .= ', i.clientid as clientid ';
	$sql .= 'From invoice i ';
	$sql .= 'INNER JOIN truckroute t ';
	$sql .= 'on i.truckid = t.id ';
	$sql .= 'INNER JOIN client c ';
	$sql .= 'on i.clientid = c.id ';
	$sql .= 'INNER JOIN invoice_remarks ir ';
	$sql .= 'on ir.invoiceno = i.invoiceno ';
	$sql .= $where;
	$sql .= 'ORDER BY DATE(ir.issuedDate) DESC ';
	$sql .= $limit;

	$stat = R::getAll ($statsql);

	if (sizeof($stat) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}
	$all = R::getAll ($sql);

	//
	//	Paging columns here
	//	
	$data['itemcount']=sizeof($all);
	$data['currentpage']=$currentPage*1;
	$data['totalitems']=$stat[0]['count'];
	$data['totalpages']=ceil($stat[0]['count'] / $pagesize);

	//
	//	Stuff data here
	//
	$data['items']=$all;

	echo json_encode($data);
});

//update existing invoice
$app->post('/update_invoice', function () use ($app) {
	
	$paramValue = json_decode($app->request()->getBody());

	$invoiceno= $paramValue->invoiceno;

	R::begin();

	//Remove original
	$sql = 'DELETE FROM invoice WHERE invoiceno = "'.$invoiceno.'"' ;
	R::exec($sql);

	$sql = 'DELETE FROM invoice_remarks WHERE invoiceno = "'.$invoiceno.'"';
	R::exec($sql);   
	
	
	$lastdate = date('Y-m-d H:i:s');

	//
	//saving data into invoice table
	//
	$sql = "INSERT INTO invoice (clientid, invoiceno, itemid, itemqty, truckid) VALUES ";

	for ($i = 0; $i <  count($paramValue->items); $i++) {

		$item_id	= $paramValue->items[$i]->itemid;
		$item_qty	= $paramValue->items[$i]->qty | 0;
				
		if ($item_qty == 0) {
			continue;
		}

		if ($i != 0) {
			$sql .= ",";
		}

		$sql .= "(".$paramValue->clientid.",'".$invoiceno."',".$item_id.",".$item_qty.",".$paramValue->truckid.")";
	}

	R::exec($sql);

	//
	//saving remarks into invoice_remarks table
	//
	$sql = "INSERT INTO invoice_remarks (invoiceno, remarks1, remarks2, remarks3, cashcredit, issuedDate, totalprice, lastupdated) VALUES ";
	$sql .= "('".$invoiceno."','".$paramValue->remarks1."','".$paramValue->remarks2."','".$paramValue->remarks3."','".$paramValue->cashcredit."','".$paramValue->issuedDate."',".$paramValue->total.",'".$lastdate."')";

	R::exec($sql);

	R::commit();
	/*  send command to dot matrix printer  */

		//
		// do something here...
		//

	echo $lastdate;

});

// register new invoice
$app->post('/register_invoice', function () use ($app) {

	//
	//Create invoice
	//

    $paramValue = json_decode($app->request()->getBody());
	
	//
	//generating unique invoice number based on the date
	//
	$tag = $paramValue->tag;

	$invoiceno= 'SP'.$tag.date ('YmdHis') ;
	$issuedDate = $paramValue->issuedDate;
	$lastdate = date ('Y-m-d H:i:s');

	//
	//saving data into invoice table
	//
	$sql = "INSERT INTO invoice (clientid, invoiceno, itemid, itemqty, truckid) VALUES ";

	for ($i = 0; $i <  count($paramValue->items); $i++) {

		$item_id	= $paramValue->items[$i]->id;
		$item_qty	= $paramValue->items[$i]->qty | 0;
				
		if ($item_qty == 0) {
			continue;
		}

		if ($i != 0) {
			$sql .= ",";
		}

		$sql .= "(".$paramValue->clientid.",'".$invoiceno."',".$item_id.",".$item_qty.",".$paramValue->truckid.")";
	}

	R::exec($sql);
	
	//
	//saving remarks into invoice_remarks table
	//
	$sql = "INSERT INTO invoice_remarks (invoiceno, remarks1, remarks2, remarks3, cashcredit, issuedDate, totalprice, lastupdated ) VALUES ";
	$sql .= "('".$invoiceno."','".$paramValue->remarks1."','".$paramValue->remarks2."','".$paramValue->remarks3."','".$paramValue->cashcredit."','".$issuedDate."',".$paramValue->total.",'".$lastdate."')";

	R::exec($sql);

	//update frequency of item

	$g = 0;
	$sql  = 'UPDATE item as it SET it.frequency = it.frequency + 1 ';
	$sql .= 'WHERE ';
	for ($i = 0; $i <  count($paramValue->items); $i++) {

		$item_id	= $paramValue->items[$i]->id;
		$item_qty	= $paramValue->items[$i]->qty | 0;
				
		if ($item_qty == 0) {
			continue;
		}

		if ($g != 0) {
			$sql .= " OR ";
		}

		$sql .= ' it.id=' . $item_id;		
		$g ++;
	}

	R::exec($sql);

	/*  send command to dot matrix printer  */

		//
		// do something here...
		//
	
	echo $invoiceno;		

});

?>