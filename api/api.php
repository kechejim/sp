<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Slim Framework
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Redbean PHP
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
require_once('Config/rb.php');
require_once('Config/rbsetup.php');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  PHP Globals
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('Asia/Hong_Kong');
session_start();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Slim routes
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$app = new \Slim\Slim();

$authenticate = function ($app) {
    return function () use ($app) {
        if (!isset($_SESSION['uid'])) {
            $app->redirect('../../#/login');
        }
    };
};

$app->get('/test', function() use ($app){
   	echo "<table border=1><tr><td>test</td><td>test</td><td>test</td><td>test</td></tr></table>";
});

function addWhere($where, $clause) {
	if ($where == "") {$where = " where ";} else {$where = $where . " and ";};
	return $where . $clause;
}
function addWhereOr($where, $clause) {
	if ($where == "") {$where = " where ";} else {$where = $where . " or ";};
	return $where . $clause;
}

//print
require('Routes/print.php');

//login
require('Routes/user.php');

//truck route
require('Routes/items.php');

//invoice 
require('Routes/invoice.php');

//client group
require('Routes/clientgroup.php');

//client
require('Routes/client.php');

//truck route
require('Routes/truck.php');

//common table pull operation
require('Routes/base.php');

$app->run();

exit;

?> 